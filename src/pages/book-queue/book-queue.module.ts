import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookQueuePage } from './book-queue';

@NgModule({
  declarations: [
    BookQueuePage,
  ],
  imports: [
    IonicPageModule.forChild(BookQueuePage),
  ],
})
export class BookQueuePageModule {}
