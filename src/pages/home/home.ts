import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SearchPage } from '../search/search';
import { BookQueuePage } from '../book-queue/book-queue';
import { QueuePage } from '../queue/queue';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  openPage() {
    this.navCtrl.push(SearchPage)
  }

  openPage2() {
    this.navCtrl.push(BookQueuePage)
  }

  openPage3() {
    this.navCtrl.push(QueuePage)
  }

}
