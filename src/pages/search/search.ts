import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
declare var google;
import { Plugins } from '@capacitor/core'
import { HomePage } from '../home/home';
const { Geolocation } = Plugins;

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  map: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  @ViewChild('map') mapElement: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap(){
    Geolocation.getCurrentPosition().then(data => {
      let latLng = { lat: data.coords.latitude, lng: data.coords.longitude }
      let mapOptions = {
        center: latLng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      var iconok = {
        url: 'assets/imgs/placeholder.svg',
        scaledSize: new google.maps.Size(30, 30),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 30)
      };
      var marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        icon: iconok
      });
      this.directionsDisplay.setMap(this.map);

      var service = new google.maps.places.PlacesService(this.map);
      service.nearbySearch({
        location: latLng,
        radius: 1000,
        type: ['hospital']
      }, (results, status) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            this.createMarker(results[i], this.map);
            // console.log(results[i])
          }
        }
      });
    }).catch(err => {
      console.log(err)
    })
  }
  createMarker(place, map) {
    var iconok = {
      url: 'assets/imgs/hospital.svg',
      scaledSize: new google.maps.Size(30, 30),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(15, 30)
    };
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: this.map,
      position: placeLoc,
      icon: iconok
    });

    google.maps.event.addListener(marker, 'click', ()=>{
      // this.navCtrl.push(HomePage)
      // window.open(map, marker);
    });
  }

  backPage() {
    this.navCtrl.push(HomePage)
  }

}
