webpackJsonp([3],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BookQueuePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the BookQueuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BookQueuePage = /** @class */ (function () {
    function BookQueuePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    BookQueuePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BookQueuePage');
    };
    BookQueuePage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    BookQueuePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-book-queue',template:/*ion-inline-start:"C:\Users\it corner\Desktop\hospTicket\src\pages\book-queue\book-queue.html"*/'<!--\n  Generated template for the BookQueuePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-title class="head">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <div (click)="backPage()">\n            <h2>\n              <ion-icon name="arrow-round-back"></ion-icon>\n            </h2>\n          </div>\n        </ion-col>\n        <ion-col>\n          <h1>จองคิว</h1>\n        </ion-col>\n        <ion-col>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-title>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <img class="img-hosp" src="/assets/imgs/nu-hosp.jpg">\n        <div class="box-hosp">\n          <p style="font-size: 15px; font-weight: bold;">โรงพยาบาลมหาวิทยาลัยนเรศวร</p>\n          <p>\n            <ion-icon name="md-pin"> โรงพยาบาลมหาวิทยาลัยนเรศวร</ion-icon>\n          </p>\n          <p>\n            <ion-icon name="md-alarm" style="color:crimson;"> รออีก 2 คิว</ion-icon>\n          </p>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label>ประเภท</ion-label>\n          <ion-select [(ngModel)]="toppings">\n            <ion-option>ทั่วไป</ion-option>\n            <ion-option>Black Olives</ion-option>\n            <ion-option>Extra Cheese</ion-option>\n            <ion-option>Mushrooms</ion-option>\n            <ion-option>Pepperoni</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label>แพทย์</ion-label>\n          <ion-select [(ngModel)]="toppings">\n            <ion-option>หมอ A</ion-option>\n            <ion-option>หมอ B</ion-option>\n            <ion-option>หมอ C</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-list>\n          <ion-item>\n            <ion-label>อาการ</ion-label>\n            <ion-input placeholder="   ...กรุณากรอกอาการเบื้อต้น...   "></ion-input>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n  <button ion-button full color="secondary">\n    <ion-icon> จองคิว </ion-icon>\n  </button>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\it corner\Desktop\hospTicket\src\pages\book-queue\book-queue.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], BookQueuePage);
    return BookQueuePage;
}());

//# sourceMappingURL=book-queue.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__capacitor_core__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Geolocation = __WEBPACK_IMPORTED_MODULE_2__capacitor_core__["a" /* Plugins */].Geolocation;
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        this.loadMap();
    };
    SearchPage.prototype.loadMap = function () {
        var _this = this;
        Geolocation.getCurrentPosition().then(function (data) {
            var latLng = { lat: data.coords.latitude, lng: data.coords.longitude };
            var mapOptions = {
                center: latLng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
            var iconok = {
                url: 'assets/imgs/placeholder.svg',
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(15, 30)
            };
            var marker = new google.maps.Marker({
                map: _this.map,
                position: latLng,
                icon: iconok
            });
            _this.directionsDisplay.setMap(_this.map);
            var service = new google.maps.places.PlacesService(_this.map);
            service.nearbySearch({
                location: latLng,
                radius: 1000,
                type: ['hospital']
            }, function (results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        _this.createMarker(results[i], _this.map);
                        // console.log(results[i])
                    }
                }
            });
        }).catch(function (err) {
            console.log(err);
        });
    };
    SearchPage.prototype.createMarker = function (place, map) {
        var iconok = {
            url: 'assets/imgs/hospital.svg',
            scaledSize: new google.maps.Size(30, 30),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 30)
        };
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: this.map,
            position: placeLoc,
            icon: iconok
        });
        google.maps.event.addListener(marker, 'click', function () {
            // this.navCtrl.push(HomePage)
            // window.open(map, marker);
        });
    };
    SearchPage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], SearchPage.prototype, "mapElement", void 0);
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"C:\Users\it corner\Desktop\hospTicket\src\pages\search\search.html"*/'<!--\n  Generated template for the SearchPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-title class="head">\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <div (click)="backPage()">\n                <h2>\n                  <ion-icon name="arrow-round-back"></ion-icon>\n                </h2>\n              </div>\n            </ion-col>\n            <ion-col>\n              <h1>ค้นหาตำแหน่ง</h1>\n            </ion-col>\n            <ion-col>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-title>\n</ion-header>\n\n<ion-content padding>\n\n  <div #map id="map"></div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\it corner\Desktop\hospTicket\src\pages\search\search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QueuePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the QueuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QueuePage = /** @class */ (function () {
    function QueuePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    QueuePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad QueuePage');
    };
    QueuePage.prototype.backPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    QueuePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-queue',template:/*ion-inline-start:"C:\Users\it corner\Desktop\hospTicket\src\pages\queue\queue.html"*/'<!--\n  Generated template for the QueuePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-title class="head">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <div (click)="backPage()">\n              <h2>\n                <ion-icon name="arrow-round-back"></ion-icon>\n              </h2>\n            </div>\n          </ion-col>\n          <ion-col>\n            <h1>คิวของคุณ</h1>\n          </ion-col>\n          <ion-col>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-title>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <img class="img-hosp" src="/assets/imgs/nu-hosp.jpg">\n        <div class="box-hosp">\n          <p style="font-size: 15px; font-weight: bold;">โรงพยาบาลมหาวิทยาลัยนเรศวร</p>\n          <p>\n            <ion-icon name="md-pin"> โรงพยาบาลมหาวิทยาลัยนเรศวร</ion-icon>\n          </p>\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <br>\n\n    <ion-row>\n      <ion-col>\n        <p class="center" style="font-weight: bold;">หมายเลขคิวของคุณ</p>\n        <h1 class="center" style="font-weight: bold;">A053</h1>\n      </ion-col>\n      <ion-col>\n        <p class="center" style="font-weight: bold;">รออีก (คิว)</p>\n        <h1 class="center" style="font-weight: bold;">4</h1>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <p class="center">\n          <ion-icon name="md-calendar"> 05/04/2019</ion-icon>\n        </p>\n        <p class="center">ประเภท</p>\n        <p class="center">แพทย์</p>\n        <p class="center">อาการ</p>\n      </ion-col>\n      <ion-col>\n        <p>\n          <ion-icon name="md-alarm"> 12:56:56 </ion-icon>\n        </p>\n        <p>ผู้ป่วยทั่วไป</p>\n        <p>หมอ A</p>\n        <p>ปวดหัวนอนไม่หลับ กระสับกระส่าย</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <button ion-button full color="danger">\n    <ion-icon> ยกเลิกการจองคิว </ion-icon>\n  </button>\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\it corner\Desktop\hospTicket\src\pages\queue\queue.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], QueuePage);
    return QueuePage;
}());

//# sourceMappingURL=queue.js.map

/***/ }),

/***/ 113:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 113;

/***/ }),

/***/ 154:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/book-queue/book-queue.module": [
		289,
		2
	],
	"../pages/queue/queue.module": [
		290,
		1
	],
	"../pages/search/search.module": [
		291,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 154;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(224);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_queue_queue__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_book_queue_book_queue__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_search_search__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_queue_queue__["a" /* QueuePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_book_queue_book_queue__["a" /* BookQueuePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_search_search__["a" /* SearchPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/book-queue/book-queue.module#BookQueuePageModule', name: 'BookQueuePage', segment: 'book-queue', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/queue/queue.module#QueuePageModule', name: 'QueuePage', segment: 'queue', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_queue_queue__["a" /* QueuePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_book_queue_book_queue__["a" /* BookQueuePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_search_search__["a" /* SearchPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import { QueuePage } from '../pages/queue/queue';
// import { BookQueuePage } from '../pages/book-queue/book-queue';
// import { SearchPage } from '../pages/search/search';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\it corner\Desktop\hospTicket\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\it corner\Desktop\hospTicket\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__search_search__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__book_queue_book_queue__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__queue_queue__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.openPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__search_search__["a" /* SearchPage */]);
    };
    HomePage.prototype.openPage2 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__book_queue_book_queue__["a" /* BookQueuePage */]);
    };
    HomePage.prototype.openPage3 = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__queue_queue__["a" /* QueuePage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\it corner\Desktop\hospTicket\src\pages\home\home.html"*/'<ion-header>\n  <ion-title class="head">\n    <img src="/assets/imgs/plus-black-symbol.svg">\n    <h1> Hospital Ticket </h1>\n  </ion-title>\n</ion-header>\n\n<ion-content padding>\n\n  <br>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col class="center" (click)="openPage()">\n        <div class="c">\n          <div class="c1">\n            <div class="c2">\n              <img src="/assets/imgs/maps-and-flags.svg">\n            </div>\n          </div>\n        </div>\n      </ion-col>\n      <ion-col class="center">\n        <div class="c">\n          <div class="c1">\n            <div class="c2">\n              <img src="/assets/imgs/user.svg">\n            </div>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col class="center">\n        <button ion-button round>ค้นหา</button>\n      </ion-col>\n      <ion-col class="center">\n        <button ion-button round>ข้อมูลส่วนตัว</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <br>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col class="center" (click)="openPage2()">\n        <div class="c">\n          <div class="c1">\n            <div class="c2">\n              <img src="/assets/imgs/add.svg">\n            </div>\n          </div>\n        </div>\n      </ion-col>\n      <ion-col class="center" (click)="openPage3()">\n        <div class="c">\n          <div class="c1">\n            <div class="c2">\n              <img src="/assets/imgs/checklist.svg">\n            </div>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col class="center">\n        <button ion-button round>จองคิว</button>\n      </ion-col>\n      <ion-col class="center">\n        <button ion-button round>คิวของคุณ</button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <br>\n\n  <button ion-button full color="danger">\n    <ion-icon> ออกจากระบบ </ion-icon>\n  </button>\n\n  <br>\n\n  <div class="wave"></div>\n  <div class="wave wave1"></div>\n  <div class="wave wave2"></div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\it corner\Desktop\hospTicket\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[201]);
//# sourceMappingURL=main.js.map